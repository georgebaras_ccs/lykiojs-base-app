import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import RegisterScreen from './components/screens/RegisterScreen';
import LoginScreen from './components/screens/LoginScreen';
import ForgotPaswordScreen from './components/screens/ForgotPaswordScreen';
import ResetPaswordScreen from './components/screens/ResetPaswordScreen';
import VerifyAccountScreen  from './components/screens/VerifyAccountScreen';
import ResendActivationScreen from './components/screens/ResendActivationScreen';

import LykioAuthorizedRoute from './components/routing/LykioAuthorizedRoute';
import Home from './components/screens/home/Home';
import Stories from './components/screens/stories/Stories';
import ContentManagement from './components/screens/contentmanagement/ContentManagement';
import UserManagement from './components/screens/usermanagement/UserManagement';
import Create from './components/screens/create/Create';
import Guides from './components/screens/guides/Guides';
import Ideology from './components/screens/ideology/Ideology';
import Settings from './components/screens/settings/Settings';
import Search from './components/screens/search/Search';
import Reports from './components/screens/reports/Reports';
import Profile from './components/screens/profile/Profile';
import Notifications from './components/screens/notifications/Notifications';
import Messages from './components/screens/messages/Messages';
import Library from './components/screens/library/Library';
import Leaderboards from './components/screens/leaderboards/Leaderboards';

const App = () => {
  return (
    <Router>
      <div className="app">
        <Switch>
          <Route exact path="/register" component={RegisterScreen}/>
          <Route exact path="/login" component={LoginScreen}/>
          <Route exact path="/forgotpassword" component={ForgotPaswordScreen}/>
          <Route exact path="/resendactivation" component={ResendActivationScreen}/>
          <Route exact path="/passwordreset/:resetToken" component={ResetPaswordScreen}/>
          <Route exact path="/verifyaccount/:verificationToken" component={VerifyAccountScreen}/>
          <LykioAuthorizedRoute exact path="/" component={Home}/>
          <LykioAuthorizedRoute exact path="/stories" component={Stories}/>
          <LykioAuthorizedRoute exact path="/contentmanagement" component={ContentManagement}/>
          <LykioAuthorizedRoute exact path="/usermanagement" component={UserManagement}/>
          <LykioAuthorizedRoute exact path="/create" component={Create}/>
          <LykioAuthorizedRoute exact path="/guides" component={Guides}/>
          <LykioAuthorizedRoute exact path="/ideology" component={Ideology}/>
          <LykioAuthorizedRoute exact path="/settings" component={Settings}/>
          <LykioAuthorizedRoute exact path="/search" component={Search}/>
          <LykioAuthorizedRoute exact path="/reports" component={Reports}/>
          <LykioAuthorizedRoute exact path="/profile" component={Profile}/>
          <LykioAuthorizedRoute exact path="/notifications" component={Notifications}/>
          <LykioAuthorizedRoute exact path="/messages" component={Messages}/>
          <LykioAuthorizedRoute exact path="/library" component={Library}/>
          <LykioAuthorizedRoute exact path="/leaderboards" component={Leaderboards}/>
        </Switch>
        </div>
    </Router>
  );
}

export default App;