import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import {AUTHORIZED_URL_PREFIX, GUIDES_URL} from '../../../Utils/Constants';
import './guides.css';
import { withNamespaces } from 'react-i18next';


const Guides = ({t,history}) => {
  const [error, setError] = useState("");
  const [guides, setGuides] = useState("");

  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }

    const fetchGuidesData = async () => {
      const storageAuthTokenValue = localStorage.getItem("authToken");
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${storageAuthTokenValue}`,
        },
      };

      try {
        console.log(`PrivateScreen-before call: about to make a call to /api/private with token: ${storageAuthTokenValue}`);
        const { data } = await axios.get(`${AUTHORIZED_URL_PREFIX}${GUIDES_URL}`, config);
        setGuides(data.data);
      } catch (error) {
        localStorage.removeItem("authToken");
        setError("You are not authorized please login");
      }
    };

    fetchGuidesData();
  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div>{guides}</div>
    </div>
  );
};

export default withNamespaces()(Guides);