import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import {AUTHORIZED_URL_PREFIX, SETTINGS_URL} from '../../../Utils/Constants';
import './settings.css';
import { withNamespaces } from 'react-i18next';
// eslint-disable-next-line
import changeLanguage from '../../../locales/translations/TranslationUtils';

const Settings = ({t,history}) => {
  const [error, setError] = useState("");
  const [settingsData, setSettingsData] = useState("");

  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }

    const fetchSettingsData = async () => {
      const storageAuthTokenValue = localStorage.getItem("authToken");
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${storageAuthTokenValue}`,
        },
      };

      try {
        console.log(`PrivateScreen-before call: about to make a call to /api/private with token: ${storageAuthTokenValue}`);
        const { data } = await axios.get(`${AUTHORIZED_URL_PREFIX}${SETTINGS_URL}`, config);
        setSettingsData(data.data);
      } catch (error) {
        localStorage.removeItem("authToken");
        setError("You are not authorized please login");
      }
    };

    fetchSettingsData();
  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <h3>{t('Change Language')}</h3>
        <div>
          <button onClick={() => changeLanguage('en')}>{t('English')}</button>
          <button onClick={() => changeLanguage('el')}>{t('Greek')}</button>
          <button onClick={() => changeLanguage('es')}>{t('Spanish')}</button>
        </div>
    <div>{settingsData}</div>
    </div>
  );
};

export default withNamespaces()(Settings);