import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import {AUTHORIZED_URL_PREFIX, HOME_URL} from '../../../Utils/Constants';
import './home.css';
import { withNamespaces } from 'react-i18next';

const Home = ({t,history}) => {
  const [error, setError] = useState("");
  const [homeData, setHomeData] = useState("");

  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }

    const fetchHomeData = async () => {
      const storageAuthTokenValue = localStorage.getItem("authToken");
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${storageAuthTokenValue}`,
        },
      };

      try {
        console.log(`PrivateScreen-before call: about to make a call to /api/private with token: ${storageAuthTokenValue}`);
        const { data } = await axios.get(`${AUTHORIZED_URL_PREFIX}${HOME_URL}`, config);
        setHomeData(data.data);
      } catch (error) {
        localStorage.removeItem("authToken");
        setError("You are not authorized please login");
      }
    };

    fetchHomeData();
  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div>{homeData}</div>
    </div>
  );
};

export default withNamespaces()(Home);