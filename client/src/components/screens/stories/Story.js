import './story.css';
import { withNamespaces } from 'react-i18next'; 

const Story = (props)=>{
    return <div className="margin-top-1em">
          <h2>{props.title}</h2>
          <img className="story-image" src={props.s3Url} alt="story"></img>
        </div>;
}

export default withNamespaces()(Story);