import './unit.css';
import { withNamespaces } from 'react-i18next'; 

const Unit = (props)=>{
    return <div className="margin-top-1em">
          <h2>{props.title}</h2>
          <a href={props.s3Url} target="_blank" rel="noreferrer">
            <img className="unit-image" src={props.coverS3Link} alt="unit"></img>
          </a>
        </div>;
}

export default withNamespaces()(Unit);