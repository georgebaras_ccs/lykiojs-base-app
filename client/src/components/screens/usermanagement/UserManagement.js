import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import {AUTHORIZED_URL_PREFIX, USER_MANAGEMENT_URL} from '../../../Utils/Constants';
import './userManagement.css';
import { withNamespaces } from 'react-i18next';

const UserManagement = ({t,history}) => {
  const [error, setError] = useState("");
  const [userManagement, setUserManagement] = useState("");

  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }

    const fetchUserManagementData = async () => {
      const storageAuthTokenValue = localStorage.getItem("authToken");
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${storageAuthTokenValue}`,
        },
      };

      try {
        console.log(`PrivateScreen-before call: about to make a call to /api/private with token: ${storageAuthTokenValue}`);
        const { data } = await axios.get(`${AUTHORIZED_URL_PREFIX}${USER_MANAGEMENT_URL}`, config);
        setUserManagement(data.data);
      } catch (error) {
        localStorage.removeItem("authToken");
        setError("You are not authorized please login");
      }
    };

    fetchUserManagementData();
  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div>{userManagement}</div>
    </div>
  );
};

export default withNamespaces()(UserManagement);
