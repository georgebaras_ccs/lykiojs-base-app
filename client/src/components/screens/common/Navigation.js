import {Link, useHistory} from 'react-router-dom';
import './navigation.css';
import { withNamespaces } from 'react-i18next';
import lykio_logo from '../../../multimedia/lykio_logo_transparent.png';


const Navigation = ({t}) => {
    const history = useHistory();
    const googleSSO = localStorage.getItem('googleSSO');

    const logoutHandler = async (e) => {
        // stop the form from submitting
        e.preventDefault();
        localStorage.removeItem("authToken");
        history.push("/login");
    } 

    return (<div className="topnav">
        <Link to="/"><span><img className="navigation-login-logo" src={lykio_logo} alt=""></img></span></Link>
        <Link to="/stories"><span>{t('Stories')}</span></Link>
        <Link to="/library"><span>Library</span></Link>
        <Link to="/leaderboards"><span>Leaderboards</span></Link>
        <Link to="/search"><span>Search</span></Link>
        <Link to="/notifications"><span>Notifications</span></Link>
        <Link to="/messages"><span>Messages</span></Link>
        <Link to="/create"><span>Create</span></Link>
        <Link to="/profile"><span>Profile</span></Link>
        <Link to="/ideology"><span>Ideology</span></Link>
        <Link to="/settings"><span>Settings</span></Link>
        <Link to="/guides"><span>Guides</span></Link>
        <Link to="/reports"><span>Reports</span></Link>
        <Link to="/usermanagement"><span>User Management</span></Link>
        <Link to="/contentmanagement"><span>Content Management</span></Link>
        {!googleSSO && <Link to="/"><span onClick={logoutHandler}>{t('Logout')}</span></Link>}  
  </div>);
}

//export default Navigation;
export default withNamespaces()(Navigation);