const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GameDataSchema = new mongoose.Schema({
    creationDate: Date
});

GameDataSchema.pre("save", async function(next){
  this.creationDate = new Date();
  next();
});

const GameData = mongoose.model("GameData", GameDataSchema);

module.exports = GameData;