const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TenantSchema = new mongoose.Schema({
    creationDate: Date
});

TenantSchema.pre("save", async function(next){
  this.creationDate = new Date();
  next();
});

const Tenant = mongoose.model("Tenant", TenantSchema);

module.exports = Tenant;