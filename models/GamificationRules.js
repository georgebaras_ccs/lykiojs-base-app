const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GamificationRulesSchema = new mongoose.Schema({
    creationDate: Date
});

GamificationRulesSchema.pre("save", async function(next){
  this.creationDate = new Date();
  next();
});

const GamificationRules = mongoose.model("GamificationRules", GamificationRulesSchema);

module.exports = GamificationRules;