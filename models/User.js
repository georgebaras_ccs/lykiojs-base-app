const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const {DEFAULT_PROFILE_PICTURE, PENDING_USER} = require("../utils/constants");

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Please provide a username"],
        unique : true
    },
    firstname: {
      type: String
    },
    lastname: {
      type: String
    },
    email: {
        type: String,
        required: [true, "Please provide email address"],
        unique: true,
        match: [
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          "Please provide a valid email"
        ],
    },
    googleId: {
      type: String
    },
    password: {
        type: String,
        required: [true, "Please add a password"],
        minlength: 6,
        select: false,
    },
    profilepicture: {
      type: String,
      default: `${DEFAULT_PROFILE_PICTURE}`
    },
    status: {
      type : String,
      default: `${PENDING_USER}`
    },
    creationDate: Date,
    resetPasswordToken: String,
    resetPasswordExpire: Date,
    stories: [{ type: Schema.Types.ObjectId, ref: 'Unit' }]
});

UserSchema.pre("save", async function(next){
  if(!this.isModified("password")){
    next();
  }
  // salt is a random string that makes the hash unpredictable
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
  
  this.creationDate = new Date();
  next();
});

UserSchema.methods.matchPasswords = async function(password){
  return await bcrypt.compare(password, this.password);
}

UserSchema.methods.getSignedToken = function(){
  return jwt.sign({id: this._id}, process.env.JWT_SECRET, {expiresIn: process.env.JWT_EXPIRATION});
}

UserSchema.methods.getResetPasswordToken = function(){
  const resetToken = crypto.randomBytes(20).toString("hex");
  this.resetPasswordToken = crypto.createHash("sha256").update(resetToken).digest("hex");
  // make the resetPassword token last for RESET_EXPIRATION_MINUTES
  this.resetPasswordExpire = Date.now()+(process.env.RESET_EXPIRATION_MINUTES*(60*1000));
  return resetToken;
}

const User = mongoose.model("User", UserSchema);

module.exports = User;