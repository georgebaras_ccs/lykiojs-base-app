const User = require('../models/User');
const ErrorResponse = require('../utils/errorResponse');
const sendEmail = require('../utils/sendEmail');
const crypto = require("crypto");
const {PENDING_USER, ACTIVE_USER} = require("../utils/constants");

exports.register = async (req, res, next) => {
    const {username, email, password} = req.body;
    try {
        const user = await User.create({
            username, email, password, status : `${PENDING_USER}`
        });
        try {
            await sendVerificationEmail(user);
        } catch (error) {
            return await resetTheProcessOnError(user, next);
        }
        res.status(200).json({
            success: true,
        });
    } catch (err) {
        next(err);
    }
}

exports.googlelogin = async (req, res, next) => {
    const {firstname, lastname, email, profilepicture, googleId} = req.body;
    try {
        const user = await User.findOne({email});
        if(!user){
            user = await User.create({
                email: email,
                username: `${firstname} ${lastname}`,
                status : `${ACTIVE_USER}`,
                password: googleId,
                googleId: googleId,
                profilepicture: profilepicture,
                firstname: firstname,
                lastname: lastname
            });
        }
        sendToken(user, 200, res);
    } catch (err) {
        next(err);
    }


}

exports.login = async (req, res, next) => {
    const {email, password} = req.body;
    if(!email || !password){
        return next(new ErrorResponse("Please provide an email and password", 400));
    }

    try {
        const user = await User.findOne({email}).select("+password");
        if(!user){
            return next(new ErrorResponse("Invalid credentials", 401));
        }
        const userExists = await user.matchPasswords(password);
        if(!userExists){
            return next(new ErrorResponse("Invalid credentials", 401));
        }
        if(user.status==='pending'){
            return next(new ErrorResponse("Go to your email to verify your account before logging in", 403));
        }
        sendToken(user, 200, res);
    } catch (err) {
        next(err);
    }
}

exports.forgotpassword = async (req, res, next) => {
    const {email} = req.body;
    try {
        const user = await User.findOne({email});
        if(!user){
            return next(new ErrorResponse("email could not be sent", 404));
        }
        const resetToken = user.getResetPasswordToken();
        // the following saves the resetToken within the user document
        await user.save();
        const resetUrl = `${process.env.FRONT_END_URL}/passwordreset/${resetToken}`;
        console.log(`Token for user ${user.email} is ${resetToken}`);
        const message = `<h1>You have requested a password reset</h1><a href="${resetUrl}" clicktracking=off>Click here to reset your password</a>`;
        
        try {
            await sendEmail({
                to: user.email,
                subject: "Lykio password reset request",
                text: message
            }); 
            res.status(200).json({success: true, data:"Reset pass email sent successfully. Go to your email to find the reset pass link"});
        } catch (error) {
            return await resetTheProcessOnError(user, next);
        }
    } catch (err) {
        next(err);
    }

}

exports.resetpassword = async (req, res, next) => {
    const {resetToken, password} = req.body;
    resetPasswordToken = crypto.createHash("sha256").update(resetToken).digest("hex");
    try {
        // TODO: check if the resetPasswordExpiration is of valid value
        const currentTimeStamp = Date.now();
        const user = await User.findOne({
            resetPasswordToken : resetPasswordToken
        });

        if(!user){
            return next(new ErrorResponse("Invalid reset token", 400));
        }

        user.password =password;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpire = undefined;

        await user.save();
        res.status(201).json({success: true, data:"Pass was reset successfully"});
    } catch (err) {
        next(err);
    }
}

exports.verifyaccount = async(req, res, next) => {
    // the db userid was used instead of encrypting data, 
    // no security need at this point, the verificationToken sent is the db userId
    const {verificationToken} = req.body;
    console.log(`will attempt to activate user with id: ${verificationToken}`);
    const userId = verificationToken;
    try {
        const user = await User.findOne({
            _id : userId
        });

        if(!user){
            return next(new ErrorResponse("Invalid verification request", 400));
        }
        if(user.status==='active'){
            return next(new ErrorResponse("Account already verified!", 400));
        }
        user.status = 'active';
        await user.save();
        res.status(201).json({success: true, data:"Your account has been activated!"});
    } catch (err) {
        next(err);
    }
};

exports.resendactivation = async (req, res, next) => {
    const {email} = req.body;
    try {
        const user = await User.findOne({
            email : email
        });
        // send verification email
        if(user.status==='active'){
            return next(new ErrorResponse("The user has already been activated", 403));
        }
        try {
            await sendVerificationEmail(user); 
        } catch (error) {
            // if there is an error we need to reset the process
            user.resetPasswordToken = undefined;
            user.resetPasswordExpire = undefined;
            await user.save();
            return next(new ErrorResponse("email could not be sent", 500));
        }

        res.status(200).json({
            success: true,
        });
    } catch (err) {
        next(err);
    }
}

const sendVerificationEmail = async(user) => {
    const verificationUrl = `${process.env.FRONT_END_URL}/verifyaccount/${user._id}`;
    console.log(`Verification url for user ${user.email} is ${verificationUrl}`);
    const message = `<h1>Welcome to Lykio.</h1><a href="${verificationUrl}" clicktracking=off>Click here to verify your account</a>`;
    await sendEmail({
        to: user.email,
        subject: "Lykio Registration Verification",
        text: message
    });
}

const sendToken = (user, statusCode, res) => {
    const token = user.getSignedToken();
    user.googleId ? true : false; 
    res.status(statusCode).json({
        success: true,
        authToken: token,
        googleSSO: user.googleId ? true : false
    });
}
async function resetTheProcessOnError(user, next) {
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
    await user.save();
    return next(new ErrorResponse("email could not be sent", 500));
}

