const S3 = require('aws-sdk/clients/s3');
const fs = require('fs');
const {promisify} = require('util');
const unlinkAsync = promisify(fs.unlink);
const ErrorResponse = require('./errorResponse');


const bucketName = process.env.AWS_BUCKET_NAME
const region = process.env.AWS_BUCKET_REGION
const accessKeyId = process.env.AWS_ACCESS_KEY
const secretAccessKey = process.env.AWS_SECRET_KEY

const s3 = new S3({
    accessKeyId,
    secretAccessKey
})

exports.uploadFileToS3 = (file) => {
    const fileContent = fs.readFileSync(file.path);
    const uploadParams = {
        Bucket: bucketName,
        Body: fileContent,
        ACL:'public-read', 
        Key: `${process.env.AWS_BUCKET_STORIES_PREFIX}${file.filename}`
    } 

s3.upload(uploadParams, async (err, data)=> {
    if(err){
    // delete the local file
        await unlinkAsync(file.path);
        throw err;
    }
        console.log(`File was uploaded to ${data.Location} with key: ${data.key}`);
        // also delete the local file
        await unlinkAsync(file.path);
    }) 
}