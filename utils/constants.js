exports.DEFAULT_PROFILE_PICTURE = "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";
exports.PENDING_USER = "pending";
exports.ACTIVE_USER = "active";
exports.LOCKED_USER = "locked";
exports.PENDING_UNIT= "pending";
exports.APPROVED_UNIT = "approved";
